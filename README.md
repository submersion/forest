# Forest

## A randomized node painter with collisions


![img](./doc/colourful.png)

I have been inspired by [@inconvergent](https://twitter.com/inconvergent) and its project [hyphae](https://inconvergent.net/generative/hyphae/). 

It is possible to create GIF:

![ball_anim.gif](./doc/ball_anim.gif) 

Multiple parameters are available to play with.

### Randomness
It is possible to set them randomly using `fill_rand = True`.
You can always access and change them after that.

A seed can bet defined to get a random but repeatable tree. It can be a string !