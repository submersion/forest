#!/usr/bin/env python

import os
import math
import cairo
from enum import Enum
from numpy import cos, sin, pi, arctan2, sqrt, power, absolute,\
                  square, int, linspace, any, all, array
from numpy.random import random, normal, randint, uniform, choice, rand
import numpy as np
import bisect

PATH = "img"

## This function allow to pick a random boolean with a custom probability
def rand_bool(true_proba=0.5) :
	if true_proba == 0.5 :
		return choice([True, False])
	else :
		return random() < true_proba

## This function allows to choose the proba when picking randomly in an enumeration
def custom_choice(enum_class, probas) : 
	if len(probas) != len(list(enum_class)) : return None
	intervals = np.cumsum(probas)
	np.insert(intervals, 0, 0.)

	return enum_class( bisect.bisect_left(intervals, random()) )


# Sources placement enum
class SP (Enum) :
	RANDOM = 0
	CIRCLE = 1

# Starting angle of sources enum
class SA (Enum) :
	RANDOM = 0
	CIRCLE = 1

# Delimiting shape enum
class SHAPE (Enum) :
	CIRCLE  = 0
	HEART   = 1
	RHOMBUS = 2

# Color gradient enum
class GRADIENT (Enum) : 
	LIGHTER = 0
	DARKER  = 1
	PERSONALIZED = 2

# Colouring method enum
class COULOUR (Enum) : 
	BY_BRANCHES = 0
	BY_GRADIENT = 1
	BY_PALETTE  = 2

# Collision method enum
class COLLISIONS (Enum) : 
	ORIG_RADIUS = 0			# Combine well with the placement with original radius
	PROP_RADIUS = 1

# Placement method enum
class PLACEMENT (Enum) :
	NORMAL = 0
	ORIG_RADIUS = 1


class Render () :
	def __init__ (self, W, H) :

		# clear the PATH folder
		os.system("rm " + PATH + "/* ")

		surface = cairo.ImageSurface(cairo.FORMAT_ARGB32, W, H)
		ctx = cairo.Context(surface)
		ctx.scale(W, H)  # Normalizing the canvas

		ctx.select_font_face("Georgia")
		ctx.set_font_size(0.01)

		self.surface = surface
		self.ctx = ctx

		# A counter of the printer images for the GIF
		self.counter = 0

	def circle_fill (self, x, y, r, color=[0,0,0], label='') :

		self.ctx.arc(x, y, r, 0, 2*pi)
		self.ctx.set_source_rgb(color[0], color[1], color[2])
		self.ctx.fill()

		x_bearing, y_bearing, width, height = self.ctx.text_extents(str(label))[:4]
		self.ctx.move_to(x - width / 2 - x_bearing,  y - height / 2 - y_bearing)
		self.ctx.set_source_rgb(color[0], color[1], color[2])
		self.ctx.show_text(str(label))

	def line (self, x1, y1, x2, y2, width, color=[0,0,0], **options) :

		self.ctx.move_to(x1, y1)
		self.ctx.line_to(x2, y2)
		self.ctx.set_line_width(width)

		# check for a second color
		to_color = options.get("to_color")

		if to_color is None : 
			#  we have a single color
			self.ctx.set_source_rgb(color[0], color[1], color[2])
		else :
			# then we have a gradient
			linear = cairo.LinearGradient(x1, y1, x2, y2)
			linear.add_color_stop_rgba(0., color[0], color[1], color[2], 1.)
			linear.add_color_stop_rgba(1., to_color[0], to_color[1], to_color[2], 1.)		
			self.ctx.set_source(linear) 

		self.ctx.stroke()

	def print (self, num, filename="image", seed=None) :

		# try : PATH
		# except NameError : PATH="test"
		if num == -1 : 
			self.surface.write_to_png(PATH + '/' + filename + "_final_seed='" + str(seed) +  "'.png")
			print("Images are in the folder " + PATH + ".")
		else :
			self.surface.write_to_png(PATH + '/' + filename + f"_{num:03d}" + '.png')
			self.counter += 1

	def print_gif (self, filename="image", seed=None) :

		filename = PATH + "/" + filename + "_" + str(seed) + "_animation.gif"
		os.system("convert -delay 10 " + PATH + "/image_*.png \( -clone -1 -set delay 200 \)  -swap -1 " + filename )
		print("I have created a GIF animation out of " + str(self.counter) + " images : " + PATH + "/" + filename)


class Tree () : 

	def __init__ ( self, 
				   render, 
				   print_that_often     = 1000,
				   speak_that_often     = 1000,
				   gif_print            = False,
				   center               = [0.5,0.5],
				   nmax                 = 10,
				   attempt_max          = 5000,
				   radius               = 0.01,
				   attempt_max_per_node = 10,
				   source_num           = 5,
				   fill_rand            = False		) :

		self.render = render  
		self.PRINT_THAT_OFTEN = print_that_often
		self.SPEAK_THAT_OFTEN = speak_that_often
		self.GIF_PRINT = gif_print

		self.CENTER                = center
		self.NMAX                  = nmax                  if not fill_rand else randint(1000, 10000)
		self.ATTEMPT_MAX           = attempt_max           if not fill_rand else randint(1000, 60000)
		self.RADIUS                = radius                if not fill_rand else random()*0.02 + 0.005
		self.ATTEMPTS_MAX_PER_NODE = attempt_max_per_node  if not fill_rand else randint(5, 15)
		self.SOURCE_NUM            = source_num            if not fill_rand else randint(1,10)


		# self.set_sources(fill_rand = True)
		# self.set_graphics 
		# ...

	def set_sources ( self, 
					  source_placement    = SP.CIRCLE,
					  source_radius       = 0.1,
					  source_gathering    = 1,
					  source_angle        = SA.CIRCLE,
					  source_angle_random = False,
					  source_color        = [[0.,0.,0.]],
					  fill_rand           = False 		) :

		self.x        = np.empty(self.SOURCE_NUM, 'float')
		self.y        = np.empty(self.SOURCE_NUM, 'float')
		self.angle    = np.empty(self.SOURCE_NUM, 'float')
		self.radius   = np.empty(self.SOURCE_NUM, 'float')
		self.parent   = np.empty(self.SOURCE_NUM, 'int')
		self.gen      = np.empty(self.SOURCE_NUM, 'int')		# number of 'Y' above the node
		self.child    = np.empty(self.SOURCE_NUM, 'bool')		# True if the node has a child, used for branches
		self.attempts = np.zeros(self.SOURCE_NUM, 'int')		# number of attempt to grew a branch at the node
		self.color    = np.array( [ np.zeros(3) for i in range(self.SOURCE_NUM) ] )
		self.family   = np.empty(self.SOURCE_NUM, 'int') 		# source node ancestor

		self.SOURCE_PLACEMENT    = source_placement     if not fill_rand else choice(list(SP))
		self.SOURCE_RADIUS       = source_radius        if not fill_rand else uniform(0.05, 0.2)
		self.SOURCE_GATHERING    = source_gathering     if not fill_rand else uniform(0.3, 1.) 		# No effect if equal to one. Make sources closer when along the circle when <1
		self.SOURCE_ANGLE        = source_angle         if not fill_rand else choice(list(SA))
		self.SOURCE_ANGLE_RANDOM = source_angle_random  if not fill_rand else rand_bool() 	# initial angle of movements of each source is random

		## Color
		# If there is only one list for the source color, I consider they are all of that same color
		if np.shape(source_color)[0] == 1 and self.SOURCE_NUM > 1 :
			source_color = np.array([ source_color[0] for i in range(self.SOURCE_NUM) ])
 
		self.SOURCE_COLOR        = np.array(source_color) if not fill_rand else rand(self.SOURCE_NUM, 3)

		# if np.shape(source_color)[0] != self.SOURCE_NUM : 
		# 	print("Wrong number of colors for sources given.")

	def set_graphics ( self,
					   std_dev                      = 0.5,
					   radius_decrease_along_branch = 0.96,
					   decrease_when_new_branch     = True,
					   radius_decrease_new_branch   = 0.96,
					   generation_threshold         = True, 	# Limit the decrease in the radius
					   max_generation               = 10,		# Number of allowed generations, ie number of consecutive 'Y' allowed per family
					   placement_of_nodes           = PLACEMENT.NORMAL,
					   link_relatives               = False,
					   color_gradient               = GRADIENT.PERSONALIZED,
					   colouring                    = COULOUR.BY_BRANCHES,
					   color_step                   = 0.03,
					   filling_gaps                 = True, 	# connect nodes with thick lines
					   shape_check                  = True, 	# delimit a shape around the tree
					   shape_contour                = SHAPE.HEART,
					   shape_radius                 = 0.3,
					   end_color                    = [[1,1,1]],
					   fill_rand                    = False                                       ) :

		self.STD_DEV                      = std_dev                      if not fill_rand else uniform(0.1,1)
		self.RADIUS_DECREASE_ALONG_BRANCH = radius_decrease_along_branch if not fill_rand else uniform(0.85, 1.03)
		self.DECREASE_WHEN_NEW_BRANCH     = decrease_when_new_branch     if not fill_rand else rand_bool()
		self.RADIUS_DECREASE_NEW_BRANCH   = radius_decrease_new_branch   if not fill_rand else uniform(0.85,1.03)
		self.GENERATION_THRESHOLD         = generation_threshold         if not fill_rand else rand_bool()
		self.MAX_GENERATION               = max_generation               if not fill_rand else randint(7,20)
		self.PLACEMENT_OF_NODES           = placement_of_nodes           if not fill_rand else choice(list(PLACEMENT))
		self.LINK_RELATIVES               = link_relatives               if not fill_rand else rand_bool()
		self.COLOR_GRADIENT               = color_gradient               if not fill_rand else custom_choice(GRADIENT, [0.2, 0.2, 0.6])
		self.COLOURING                    = colouring                    if not fill_rand else choice(list(COULOUR))
		self.COLOR_STEP                   = color_step                   if not fill_rand else uniform(0.01, 0.05)
		self.FILLING_GAPS                 = filling_gaps                 if not fill_rand else rand_bool(0.8)
		self.SHAPE_CHECK                  = shape_check                  if not fill_rand else rand_bool(0.1)
		self.SHAPE_CONTOUR                = shape_contour                if not fill_rand else custom_choice(SHAPE, [0.7,0.1,0.2])
		self.SHAPE_RADIUS                 = shape_radius                 if not fill_rand else random()*0.5

		# If there is only one list for the source color, I consider they are all of that same color
		if np.shape(end_color)[0] == 1 and self.SOURCE_NUM > 1 : 
			self.END_COLOR = np.array([ end_color[0] for i in range(self.SOURCE_NUM) ])

		self.END_COLOR                    = np.array(shape_radius)       if not fill_rand else rand(self.SOURCE_NUM, 3)


	def set_collisions ( self,
						 collisions_scheme = COLLISIONS.PROP_RADIUS,
						 scaling_collisions_factor = 0.8,
					     fill_rand=False) : 
		self.COLLISIONS_SCHEME = collisions_scheme if not fill_rand else choice(list(COLLISIONS))

		self.SCALING_COLLISIONS_FACTOR = scaling_collisions_factor if not fill_rand else uniform(0.7,1.05)	# >1: allow for covering 	<1: spaces the branches 


	def set_printing (self) :

		self.PATH = 'img'
		self.WIDTH, HEIGHT = 1024, 1024
		self.GIF_PRINT = True
		self.PRINT_THAT_OFTEN


	def is_in_shape (self, x, y) :

		if self.SHAPE_CONTOUR == SHAPE.CIRCLE :
			if square(x - 0.5) + square(y - 0.5) > square(self.SHAPE_RADIUS) :
				return False
			else :
				return True
		elif self.SHAPE_CONTOUR == SHAPE.HEART :
			# I compute the angle made by the target node 
			# with the center of the canvas 
			angle_with_center = arctan2(0.3 - y, 0.5 - x)
			if square(x - 0.5) + square(y - 0.3) > square (0.15 * ( sin(angle_with_center)*sqrt(abs(cos(angle_with_center))) / (sin(angle_with_center) + 7./5.) - 2*sin(angle_with_center) + 2 ) ) : 
				return False
			else :
				return True
		elif self.SHAPE_CONTOUR == SHAPE.RHOMBUS :
			# I change to a coordinate system centered in (0.5, 0.5)
			x = x - 0.5
			y = 1-y - 0.5
			return y > x-0.5 and y < x+0.5 and y < -x+0.5 and y > -x-0.5 
		else :
			print("Shape unknown.")
			raise ValueError


	def is_in_canvas (self, x, y, r) : 
		return not (x-r > 1 or x+r < 0 or y-r > 1 or y+r < 0 )


	def place_sources (self) : 
		
		## Let me defined the initial sources positions
		if self.SOURCE_PLACEMENT == SP.CIRCLE :

			first_source = random() * 2*pi
			self.sources_theta = first_source + np.arange(self.SOURCE_NUM) * 2.*pi/float(self.SOURCE_NUM) * self.SOURCE_GATHERING
			self.sources_rho = np.ones(self.SOURCE_NUM) * self.SOURCE_RADIUS

		elif self.SOURCE_PLACEMENT == SP.RANDOM :

			self.sources_theta = random(self.SOURCE_NUM) * 2*pi
			self.sources_rho = random(self.SOURCE_NUM) * 0.5


		## I fill the relevant array and print the sources on the canvas
		for i, theta in enumerate(self.sources_theta) :

			self.x[i]      = 0.5 + self.sources_rho[i]*cos(theta)
			self.y[i]      = 0.5 + self.sources_rho[i]*sin(theta)
			self.radius[i] = self.RADIUS
			self.parent[i] = -1
			self.gen[i]    = 0
			self.child[i]  = False
			self.family[i] = i

			self.color[i]  = self.SOURCE_COLOR[i]

			if self.SOURCE_ANGLE == SA.RANDOM : 
				self.angle[i] = random() * 2*pi
			elif self.SOURCE_ANGLE == SA.CIRCLE : 
				self.angle[i] = theta

			# If a source node is outside the shape, I don't print it (but I have to append it to the arrays to )
			if self.is_in_shape(self.x[i], self.y[i]) : 
				self.render.circle_fill(self.x[i], self.y[i], self.RADIUS, self.color[i]) 
			

	def collide (self, x, y, r, parent) :

		# Computing the distances to other nodes
		distances = square(self.x - x) + square(self.y - y)

		if self.COLLISIONS_SCHEME == COLLISIONS.ORIG_RADIUS :
			# collision with the RADIUS value 	->	 sparser
			colliding_mask = distances > np.ones(len(self.x),'float') * square(2*self.RADIUS)
		elif self.COLLISIONS_SCHEME == COLLISIONS.PROP_RADIUS :
			# this is the true collisions : with a scale collision factor of 1, 
			# we observe tangent circles 
			colliding_mask = ( self.SCALING_COLLISIONS_FACTOR*distances > square(self.radius + r))

		# I remove the parent node from the collision check
		colliding_mask = colliding_mask[np.arange(len(colliding_mask))!=parent]

		## I return False, if every node is far enough of the target one
		return not colliding_mask.all()


	def grow (self) : 

		## I compute the constant that rely on the tunable parameters here 
		## This avoid them to remain unchanged while a parameter is tuned by hand 'after' calling the setting function

		self.place_sources()
		self.COLOR_DIFF = self.END_COLOR - self.SOURCE_COLOR

		## This is the main loop

		for n in range (1, self.ATTEMPT_MAX) :

			if n % self.SPEAK_THAT_OFTEN == 0 :
				print('Let us try node', n)

			## I take a random existing node as a candidate parent. 
			## I call it 'k'

			k = int(random() * len(self.x))

			# The node dies if it reaches the maximum authorized attempts
			self.attempts[k] += 1
			if self.attempts[k] > self.ATTEMPTS_MAX_PER_NODE :
				continue

			## I compute the features of the target node

			angle_target = self.angle[k] + normal(0, self.STD_DEV)

			if self.PLACEMENT_OF_NODES == PLACEMENT.NORMAL :
				x_target = self.x[k] + cos(angle_target)*2*self.radius[k]*self.RADIUS_DECREASE_ALONG_BRANCH
				y_target = self.y[k] + sin(angle_target)*2*self.radius[k]*self.RADIUS_DECREASE_ALONG_BRANCH
			elif self.PLACEMENT_OF_NODES == PLACEMENT.ORIG_RADIUS : 
				x_target = self.x[k] + cos(angle_target)*2*self.RADIUS
				y_target = self.y[k] + sin(angle_target)*2*self.RADIUS

			# I check as soon as possible if the node is inside the printing area
			if self.SHAPE_CHECK and not self.is_in_shape(x_target, y_target) : 
				continue

			if self.child[k] : 
				gen_target = self.gen[k] + 1
			else : 
				gen_target = self.gen[k]

			family_target = self.family[k]

			radius_target = self.radius[k] * self.RADIUS_DECREASE_ALONG_BRANCH

			if self.child[k] and self.DECREASE_WHEN_NEW_BRANCH : 
				radius_target *= self.RADIUS_DECREASE_NEW_BRANCH

			## Colouring of the target node

			if self.COLOURING == COULOUR.BY_BRANCHES :
				color_target = np.array([0., 0., 0.]) + 0.1*gen_target

			if self.COLOR_GRADIENT == GRADIENT.LIGHTER : 
				color_target = self.color[k] + self.COLOR_STEP
			elif self.COLOR_GRADIENT == GRADIENT.DARKER :
				color_target = self.color[k] - self.COLOR_STEP
			elif self.COLOR_GRADIENT == GRADIENT.PERSONALIZED :
				color_target = self.color[k] + self.COLOR_STEP*self.COLOR_DIFF[family_target]


			# I ensure the value of the color is in the [0,1] rgb range
			for i, c in enumerate(color_target) :
				if c > 1. : 
					color_target[i] = 1.
				elif c < 0. :
					color_target[i] = 0.

			## Now is time for collision : does the node collide
			## with the other ones w.r.t. the chosen collisions rules ?

			added_new_node = False

			if self.collide(x_target, y_target, radius_target, k) : 
				continue
			else :
				# I push the computed values to the arrays
				self.x        = np.append(self.x, x_target)
				self.y        = np.append(self.y, y_target)
				self.angle    = np.append(self.angle, angle_target)
				self.radius   = np.append(self.radius, radius_target)
				self.parent   = np.append(self.parent, k)
				self.gen      = np.append(self.gen, gen_target)
				self.child    = np.append(self.child, False)
				self.attempts = np.append(self.attempts, 0)
				self.color    = np.append(self.color, [color_target], axis=0)
				self.family   = np.append(self.family, family_target)

				# If the parent had no child, I change that
				if not self.child[k] : 
					self.child[k] = True

				# And paint the node on the canvas, if it is visible on it 
				if self.is_in_canvas(x_target, y_target, radius_target) :
					self.render.circle_fill(x_target, y_target, radius_target, color_target)

				if self.FILLING_GAPS :
					self.render.line(self.x[k], self.y[k], x_target, y_target, 2*radius_target, self.color[k], to_color = color_target)

				if self.LINK_RELATIVES : 
					self.render.line(self.x[k], self.y[k], x_target, y_target, 0.001, [1,0,0])

				added_new_node = True

			## I print an image if a node has been added
			## and if it is the right time to do it

			if added_new_node and len(self.x) % self.PRINT_THAT_OFTEN == 0 and self.GIF_PRINT :
				self.render.print(int(len(self.x)/self.PRINT_THAT_OFTEN))

			## If the maximum number of nodes is reached, I break the loop
			if len(self.x) > self.NMAX : 
				print("Maximum number of " + str(self.NMAX) + " nodes reached")
				print("Note that max. attempts was " + str(self.ATTEMPT_MAX))
				break

		## For loop has ended, let's say it
		print("".join([" - " for i in range (20)]))
		print("From " + str(self.SOURCE_NUM) + " sources, I have painted " + str(len(self.x)) + " nodes, out of the " + str(self.NMAX) + " authorized.")
		print("But I have attempted to place " + str(self.ATTEMPT_MAX) + " nodes in total.")


	# def __print__ :


def main () :

	seed = "Flaubert"

	GIF = False

	# I change convert the seed to a list of int
	try : 
		if type(seed) is str : 
			acceptable_seed = [ ord(char) for char in seed ]
		elif type(seed) is int :
			acceptable_seed = seed
		elif seed == None :
			acceptable_seed = None
	except UnboundLocalError :
		print("No seed defined, setting it to None.")
		acceptable_seed = None

	# I set the seed
	np.random.seed(acceptable_seed)

	# Creation of the render object
	size = 1000
	render = Render(size, size)

	# Creation of the tree
	tree = Tree(render, gif_print = GIF, fill_rand=True) ## !! CARFEFUL !! rand override the value precise before 
	tree.ATTEMPT_MAX=50000 	# ensure lots of tried nodes
	tree.PRINT_THAT_OFTEN = 100

 	# I set the parameters of the tree 
	tree.set_sources(fill_rand=True)
	tree.set_graphics(fill_rand=True)
	tree.set_collisions(fill_rand=True)

	# Unaesthetic paramter (maybe remove it ?)
	tree.LINK_RELATIVES = False

	# Verbose of the parameters' value of the tree
	for key, value in vars(tree).items() : 
		print( "{} : {}".format(key, value) )		

	# Grow seed, grow !!
	tree.grow()

	## Printing of the canvas by the render
	render.print(-1, seed=seed)
	if GIF :
		render.print_gif()	

	print("I have used the seed '" + str(seed) + "' to generate this tree.")


main()